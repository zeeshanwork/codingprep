'use strict'

function Merge(inputList, start, mid, end){
    let leftLen = mid - start + 1;
    let rightLen = end - mid;

    let leftArr = [];
    let rightArr = [];
    let i,j,k = 0;

    for(i = start; i <= mid; i++) {
        leftArr[k] =  inputList[i];
        k++;
    }

    k=0;
    for(j = mid+1; j <= end; j++) {
        rightArr[k] =  inputList[j];
        k++;
    }

    i=0;
    j=0;
    k=start;
    while ( i < leftLen && j < rightLen) {
        if(leftArr[i] <= rightArr[j]) {
            inputList[k] = leftArr[i];
            k++;
            i++;
        } else {
            inputList[k] = rightArr[j];
            k++;
            j++;

        }
    }

    while(i < leftLen) {
        inputList[k] = leftArr[i];
        k++;
        i++;
    }

    while(j < rightLen) {
        inputList[k] = rightArr[j];
        k++;
        j++;
    }
}


function MergeSort(inputList, start, end) {

    if(start >= end) {
        return null;
    }

    let mid = Math.floor( start + (end - start) / 2);

    MergeSort(inputList,start, mid);
    MergeSort(inputList,mid + 1, end);
    Merge(inputList, start, mid, end);
}


let inputList = [3,9,4,6,5,2,8,3];
console.log(inputList);
MergeSort(inputList, 0, inputList.length - 1);
console.log(inputList);

inputList = [];
console.log(inputList);
MergeSort(inputList, 0, inputList.length - 1);
console.log(inputList);

inputList = [9,8,7,6,5,4,3,2,1,1,2,3,4,5,6,7,8,9,0];
console.log(inputList);
MergeSort(inputList, 0, inputList.length - 1);
console.log(inputList);

inputList = [-9,-8,-2,-7,-5,-6,-3,-4,-1,1,2,3,4,5,6,7,8,9,0];
console.log(inputList);
MergeSort(inputList, 0, inputList.length - 1);
console.log(inputList);
