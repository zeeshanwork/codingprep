'use strict'

function doPartition(inputList, start, end){

    let pivot = start;
    let less_count =0;
    for(let i=start; i<=end; i++) {
        if(inputList[i] <= inputList[pivot]) {
            less_count++;
        }
    }

    let tempArray = [];
    for(let i=0; i< inputList.length; i++) {
        tempArray[i] = inputList[i];
    }

    inputList[less_count-1] = tempArray[pivot];
    let le = 0;
    let ge = less_count;
    // console.log(tempArray);
    for(let i=start; i<=end; i++) {

        if(tempArray[i] < tempArray[pivot]) {
            inputList[le++] = tempArray[i];
        }
        else if(i == pivot) {
            continue;
        } else {
            inputList[ge++] = tempArray[i];
        }
    }

    return less_count-1;
}

function doPartitionByLumotosAlgo(inputList, start, end){

    let pivot_index = end;
    let index = start;
    for(let current=start; current <= end -1; current++) {
        if(inputList[current] <= inputList[end]) {
            let tempVal = inputList[index];
            inputList[index] = inputList[current];
            inputList[current] = tempVal;
            index++;
        }
    }

    let tempVal2 = inputList[index];
    inputList[index] = inputList[end];
    inputList[end] = tempVal2;

    return pivot_index;
}

function doPartitionByHoaresAlgo(inputList, start, end){

    let pVal = inputList[start];
    let i=start;
    let j=end;
    
    while (i<j) {
        while (i<=end && inputList[i] <= pVal) {
            i++;
        }
        while (j>=start && inputList[j] > pVal) {
            j--;
        }

        if(i < j) {
            let tempVal = inputList[i];
            inputList[i] = inputList[j];
            inputList[j] = tempVal;
        }
    }

    inputList[start] = inputList[j];
    inputList[j] = pVal;

    return j;
}


function QuickSort(inputList, start, end) {

    if(start >= end) {
        return null;
    }
    let pivot = doPartitionByHoaresAlgo(inputList, start,end);
    QuickSort(inputList,start, pivot-1);
    QuickSort(inputList,pivot+1, end);
}


let inputList = [7,3,9,4,2];
console.log(inputList);
QuickSort(inputList, 0, inputList.length - 1);
console.log(inputList);

inputList = [];
console.log(inputList);
QuickSort(inputList, 0, inputList.length - 1);
console.log(inputList);

inputList = [9,8,7,6,6,7,8,9,0];
console.log(inputList);
QuickSort(inputList, 0, inputList.length - 1);
console.log(inputList);

inputList = [-9,-8,-7,-6,-5,-4,-3,-2,-1,1,2,3,4,5,6,7,8,9,0];
console.log(inputList);
QuickSort(inputList, 0, inputList.length - 1);
console.log(inputList);
